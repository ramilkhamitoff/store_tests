import Page from "./Page";
import MainPage from "./MainPage";

class CartPage extends Page {

    public get removeFromCart() { return $$('.remove-from-cart [type="checkbox"]'); }
    public get checkBoxes() { return $$('.remove-from-cart [type="checkbox"]'); }
    public get updateCart() { return $('[name="updatecart"]'); }
    public get countProduct() { return $('.qty input'); }
    public get subTotal() { return $$('.subtotal span'); }
    public get cartItemRowAttributes() { return $$('.cart-item-row .attributes') }
    /**
     * @description Метод для очистки корзины
     * 
     */
    async cleanCart() {
        try {
            if (await this.removeFromCart[0]) {
                for (let i = 0; i < await this.checkBoxes.length; i++) {
                    await this.checkBoxes[i].click();
                }
                await this.updateCart.click();
            }
        } catch (err) {
            console.error('ERROR: cleanCart!', err)
            throw Error(err);
        }

    }

    /**
     * @description Метод для открытия корзины
     * 
     */
    async openCart() {
        try {
            await MainPage.topCartLink.click();
            await browser.waitUntil(async () => await this.cartItemRowAttributes.length > 0, { timeout: 10000 });
        } catch (err) {
            console.error('ERROR: openCart!', err)
            throw Error(err);
        }

    }
}
export default new CartPage