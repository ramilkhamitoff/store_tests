
import Page from './Page';

/**
 * Класс страницы авторизации
 */
class ComputersPage extends Page {

    public get options() {
        return $$('.product-essential .attributes .option-list');
    }

    public get mainLabels() {
        return $$('.product-essential .attributes dt label');
    }

    public get price() { return $('.product-price span') }

    public get addToCart() { return $('.add-to-cart-panel input[value="Add to cart"]') }


    /**
     * @description Метод для выбора из комбобокса с вложением
     * @param {string} value1
     * @param {string} value2
     * @returns {Promise<string>}
     */
    public async selectFromMultiCombobox(value1: string, value2: string): Promise<string> {
        try {
            await browser.waitUntil(async () => await this.options.length > 0, { timeout: 10000 })
            let value: string;
            for (let i = 0; i < await this.options.length; i++) {

                if (await this.mainLabels[i].getText() === value1) {
                    for (let j = 0; j < await this.options[i].$$('li').length; j++) {

                        if (await this.stringStartWith(await this.options[i].$$('li')[j].$('label').getText(), value2)) {
                            value = await this.options[i].$$('li')[j].$('label').getText()
                            await this.options[i].$$('li')[j].$('label').click()
                            return value;
                        }

                    }
                }

            }
        } catch (err) {
            console.error('ERROR: selectFromMultiCombobox!', err);
            throw Error(err);
        }


    }
    /**
     * @description Метод для добавления товара в корзину
     */
    public async addProductToCart() {
        try {
            await this.addToCart.click();
            await browser.waitUntil(async () => await $('.content').getText() === 'The product has been added to your shopping cart',
                { timeout: 5000, timeoutMsg: await $('.content').getText() })
        } catch (err) {
            console.error('ERROR: addProductToCart!', err)
            throw Error(err)
        }
    }

}

export default new ComputersPage();
