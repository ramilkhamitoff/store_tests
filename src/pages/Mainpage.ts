import Page from './Page';

/**
 * Класс главной страницы
 */
class MainPage extends Page {

    public get account() {
        return $$('.account');
    }

    public get nivoSlider() {
        return $('[id="nivo-slider"]');
    }
    

    public get blockCategoryNavigationList() { return $$('.block-category-navigation .list li a') }

    public get topCartLink() { return $('[id="topcartlink"]') }

    public get headerLogo() { return $('.header-logo') }

    public get subCategoryItems() { return $$('.sub-category-grid .item-box .title a')}

    async openMainPage(){
        await browser.url(browser.config.baseUrl)
        await browser.waitUntil(async() => await this.nivoSlider.isDisplayed(), {timeout: 10000})
    }
}

export default new MainPage();
