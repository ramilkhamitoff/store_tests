import Page from "./Page";

class ProductsPage extends Page {

    public get addToCart() { return $$('[value="Add to cart"]') }
   
}
export default new ProductsPage