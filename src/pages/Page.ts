/**
* Класс базовых операций
*/
export default class Page {
    /**
    * @description Метод открытия главной страницы сайта
    * @param {string} path path of the sub page (e.g. /path/to/page.html)
    */
    public open(path: string) {
        return browser.url(path);
    }

    /**
     * @description Выбор элемента из списка
     * @param {Array<WebdriverIO.Element>} list 
     * @param {string} value 
     * @param {WebdriverIO.Element} mainElement - элемент для раскрытия выпадающего списка
     * @return {Promise<void>}
     */
    public async selectFromList(list: Array<WebdriverIO.Element>, value: string, mainElement?: WebdriverIO.Element): Promise<void> {
        try {
            if (mainElement) {
                mainElement.click()
            }
            for (let i = 0; i < list.length; i++) {
                if (await list[i].getText() === value) {
                    await list[i].click();
                    break;
                }
            }
        } catch (err) {
            console.error('ERROR! selectFromList', err);
            throw Error(err);
        }

    }

    /**
     * @description Выбор элемента из списка
     * @param {string} str1 
     * @param {string} str2
     * @return {Promise<boolean>}
     */
    public async stringStartWith(str1: string, str2: string): Promise<boolean> {
        try {
            const str = str1.split('').slice(0, str2.length).join('')
            if (str === str2) {
                return true
            }
            return false
        } catch (err) {
            console.error('ERROR! selectFromList', err);
            throw Error(err);
        }

    }

    /**
     * @description Проверка наличия элемента в строке
     * @param {string} str 
     * @param {string} value
     * @return {Promise<boolean>}
     */
    public async stringContains(str: string, value: string): Promise<boolean> {
        try {
            if (str.split('').indexOf(value) >= 0) {
                return true
            } return false
        } catch (err) {
            console.error('ERROR! selectFromList', err);
            throw Error(err);
        }

    }

}


