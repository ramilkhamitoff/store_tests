
import Page from './Page';

/**
 * Класс страницы авторизации
 */
class LoginPage extends Page {

    public get inputEmail() {
        return $('#Email');
    }

    public get inputPassword() {
        return $('#Password');
    }

    public get btnSubmit() {
        return $$('[type="submit"]');
    }

    public get fieldEmailError() {
        return $('.field-validation-error [for="Email"]')
    }

    public get loginError() {
        return $('.validation-summary-errors')
    }

    /**
     * @description Метод для авторизации
     * @param {string} email
     * @param {string} password
     * @returns {Promise<void>}
     */
    public async login(email: string, password: string): Promise<void> {
        await this.inputEmail.setValue(email);
        await this.inputPassword.setValue(password);
        await this.btnSubmit[1].click();
    }

}

export default new LoginPage();
