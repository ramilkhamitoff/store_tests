const fs = require("fs");
const PROJECT_PREFIX = "DS-";

function getShouldAutomatedTests(qaseIOJson) {
    const shouldAutomatedTests = {};
    for (let i = 0; i < qaseIOJson.suites.length; i++) {

        for (let j = 0; j < qaseIOJson.suites[i].cases.length; j++) {
            if (qaseIOJson.suites[i].cases[j].automation === 'automated' || qaseIOJson.suites[i].cases[j].automation === 'to-be-automated') {
                shouldAutomatedTests[qaseIOJson.suites[i].cases[j].title] = PROJECT_PREFIX + qaseIOJson.suites[i].cases[j].id
            }
        }
    }
    return shouldAutomatedTests;
}

function givMeFiles(dir, files) {

    files = files || [];
    let allFiles = fs.readdirSync(dir);
    for (let i = 0; i < allFiles.length; i++) {
        let name = dir + '/' + allFiles[i];
        if (fs.statSync(name).isDirectory()) {
            givMeFiles(name, files);
        } else {
            files.push(name);
        }
    }
    return files;
};

const files = givMeFiles('./test/specs')

function getIdFromTests(files) {
    const arr = [];
    for (let i = 0; i < files.length; i++) {
        const id = fs.readFileSync(files[i], 'utf-8').match(/(?<=[ID=])(DS-\d)/g);
        arr.push(id);
    }
    return arr.flat(Infinity)
}
let idsQase = getShouldAutomatedTests(JSON.parse(fs.readFileSync("./coverage/qase.json")));
let idsTests = getIdFromTests(files);

function isAutomated(idsQase, idsTests) {
    const obj = {};
    const arrValues = Object.values(idsQase);
    for (let i = 0; i < arrValues.length; i++) {
        if (arrValues.indexOf(idsTests[i]) >= 0) {
            obj[Object.values(idsQase)[i] + ' ' + Object.keys(idsQase)[i]] = true;
        } else {
            obj[Object.values(idsQase)[i] + ' ' + Object.keys(idsQase)[i]] = false;
        }
    }
    return obj;
}
const isAutoObj = isAutomated(idsQase, idsTests);
if (fs.existsSync('./coverage/coverage.json')) {
    fs.unlinkSync('./coverage/coverage.json')
}
fs.writeFile('./coverage/coverage.json', JSON.stringify(isAutoObj, 'utf-8'), {
    flag: 'a+'
}, err => {})