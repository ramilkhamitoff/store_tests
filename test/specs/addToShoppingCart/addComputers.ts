import { assert } from "chai";
import CartPage from "../../../src/pages/CartPage";
import ComputersPage from "../../../src/pages/ComputersPage";
import LoginPage from "../../../src/pages/LoginPage";
import MainPage from "../../../src/pages/MainPage";
import ProductsPage from "../../../src/pages/ProductsPage";
import users from "../../../testData/users";
import productAttributes from "../../../testData/productAttributes";

const actValues = [];
const resultKeys = [];
const resultValues = [];
const pricesPlus = [];

describe('Добавление в корзину компьютера', async () => {

    before(async () => {
        await LoginPage.open('login');
        await LoginPage.login(users.user1.email, users.user1.password);
    })

    beforeEach(async () => {
        await browser.url('cart');
        await CartPage.cleanCart();
        await MainPage.openMainPage();
    })

    it('Позитивный тест на десктопный компьютер', async () => {
        await MainPage.selectFromList(await MainPage.blockCategoryNavigationList, 'Computers');
        await MainPage.selectFromList(await MainPage.subCategoryItems, 'Desktops');
        await ProductsPage.addToCart[0].click();

        for (let i = 0; i < productAttributes.attributes1.keys.length; i++) {
            actValues.push(await ComputersPage.selectFromMultiCombobox(productAttributes.attributes1.keys[i], productAttributes.attributes1.values[i]));
        }

        let price = parseFloat(Number(await ComputersPage.price.getText()).toFixed(2));

        await ComputersPage.addProductToCart();
        await CartPage.openCart();

        const compArr = (await CartPage.cartItemRowAttributes[await CartPage.cartItemRowAttributes.length - 1].getText()).split('\n');

        for (let i = 0; i < compArr.length; i++) {
            resultKeys.push(compArr[i].split(': ')[0]);
            resultValues.push(compArr[i].split(': ')[1]);
        }

        for (let i = 0; i < productAttributes.attributes1.values.length; i++) {
            assert.equal(resultValues[i], actValues[i]);
            assert.equal(resultKeys[i], productAttributes.attributes1.keys[i]);
        }

        for (let i = 0; i < actValues.length; i++) {
            if (await ComputersPage.stringContains(actValues[i], '+')) {
                pricesPlus.push(actValues[i].split('+')[1].match(/\d*\.?\d*/)[0]);
            }
        }

        for (let i = 0; i < pricesPlus.length; i++) {
            price += parseFloat(Number(pricesPlus[i]).toFixed(2));
        }

        const count = Number(await CartPage.countProduct.getValue());
        assert.equal(price * count, parseFloat(Number(await CartPage.subTotal[1].getText()).toFixed(2)));
    })

})