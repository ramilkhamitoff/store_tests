import LoginPage from  '../../../src/pages/LoginPage';
import MainPage from '../../../src/pages/MainPage';
import users from '../../../testData/users'

describe('Авторизация', () => {
    it('Авторизация с валидными значениями, ID=DS-1', async () => {
        await LoginPage.open('login');

        await LoginPage.login(users.user1.email, users.user1.password);
        await expect(await MainPage.account[0].getText()).toEqual(users.user1.email);
        
    });
});


