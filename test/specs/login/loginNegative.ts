import { assert } from 'chai'
import LoginPage from '../../../src/pages/LoginPage';
import users from '../../../testData/users'
const emails = ['12345', '@MediaList.ru', '123@mailru', '@mail.ru', '$#йцукен', 'qwerty', 'йцукен', '%$3()', '@&()'];

describe('Авторизация', () => {
    it('Авторизация с невалидным email-ом, ID=DS-2', async () => {
        await LoginPage.open('login');
        for (let i = 0; i < emails.length; i++) {
            await LoginPage.inputEmail.setValue(emails[i]);
            await LoginPage.inputPassword.click();
            assert.equal(await LoginPage.fieldEmailError.getText(), 'Please enter a valid email address.');
        }

    });

    it('Авторизация с невалидным паролем, ID=DS-3', async () => {
        await LoginPage.open('login');
        await LoginPage.login(users.user1.email, '12345678')
        assert.equal(await LoginPage.loginError.getText(), `Login was unsuccessful. Please correct the errors and try again.\nThe credentials provided are incorrect`);
    });
});


